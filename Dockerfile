FROM rust:1.51.0-slim-buster as builder

WORKDIR /app
ADD Cargo.toml .
ADD Cargo.lock .
RUN mkdir -p src
RUN echo "fn main() {}" > src/main.rs
RUN cargo build --release

ADD src src
RUN touch src/main.rs
RUN cargo build --offline --release

FROM debian:buster-slim
COPY --from=builder /app/target/release/api /bin/api
ENTRYPOINT ["/bin/api"]