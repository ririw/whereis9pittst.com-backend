use anyhow::{anyhow, Context, Result};
use log::{error, info};
use rouille::{router, Request, Response};
use std::collections::HashMap;
use std::io::Read;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant};

#[macro_use]
extern crate ureq;

fn force_unwraps() {
    std::env::var("MAPBOX_KEY").context("Provide MAPBOX_KEY env var").unwrap();
    std::env::var("ROOM_ID").context("Provide ROOM_ID env var").unwrap();
    std::env::var("LIFX_TOKEN").context("Provide LIFX_TOKEN env var").unwrap();
}

fn main() {
    pretty_env_logger::init();
    force_unwraps();
    let password = std::env::var("WIPS_PASS")
        .context("Provide WIPS_PASS env var")
        .unwrap();
    let port = std::env::var("PORT")
        .unwrap_or("8080".to_string());

    let data = Arc::new(Mutex::new(Vec::new()));
    let cache = Arc::new(Mutex::new(HashMap::new()));

    let srv_string = format!("0.0.0.0:{}", port);
    println!("Starting API server on {}", srv_string);

    rouille::start_server(srv_string, move |request| {
        let mut inner_cache = cache.lock().unwrap();
        router!(request,
            // first route
            (POST) (/flash) => {
                flash_route(request, Arc::clone(&data), &password)
            },
            _ => maybe_map(request, &mut inner_cache)
        ).with_additional_header("Access-Control-Allow-Origin", "https://whereis9pittst.com")
    });
}

fn maybe_map(request: &Request, cache: &mut HashMap<String, (String, Vec<u8>)>) -> Response {
    fn bail(req: &Request) -> Response {
        error!("Bad request: {:?}", req);
        return Response::empty_404();
    }

    if request.method() != "GET" {
        return bail(request);
    }

    if !request.url().starts_with("/map") {
        return bail(request);
    }

    match map_req(request.raw_url().to_owned(), cache) {
        Ok((ctype, content)) => Response::from_data(ctype, content).with_public_cache(3600 * 24),
        Err(e) => {
            error!("Could not fetch from mapbox - {:?}", e);
            Response::text("Error during map fetch").with_status_code(500)
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum MapReqError {
    NoAPIKey(std::env::VarError),
    URLWithQn(String),
    APIError(String),
    TooManyCached,
}

fn map_req(
    raw_url: String,
    cache: &mut HashMap<String, (String, Vec<u8>)>,
) -> std::result::Result<(String, Vec<u8>), MapReqError> {
    if cache.len() > 100 {
        return Err(MapReqError::TooManyCached);
    }
    if let Some(r) = cache.get(&raw_url) {
        return Ok(r.to_owned());
    }

    let subpart = raw_url.split_at(5).1;
    if subpart.contains('?') {
        return Err(MapReqError::URLWithQn(format!(
            "Unable to process URL with '?' char: {}",
            raw_url
        )));
    }
    let auth_token = std::env::var("MAPBOX_KEY").map_err(MapReqError::NoAPIKey)?;
    let new_url = format!(
        "https://api.mapbox.com/{}?access_token={}",
        subpart, auth_token
    );
    info!("Fetch from {}", new_url);
    let resp = ureq::get(&new_url).call();

    if resp.ok() {
        let mut content = Vec::<u8>::new();
        let ctype = resp.content_type().to_owned();
        resp.into_reader().read_to_end(&mut content).unwrap();
        cache.insert(raw_url, (ctype.clone(), content.clone()));
        Ok((ctype, content))
    } else {
        Err(MapReqError::APIError(format!(
            "Could not fetch from mapbox API due to {:?}",
            resp
        )))
    }
}

fn flash_route(request: &Request, data: Arc<Mutex<Vec<Instant>>>, password: &str) -> Response {
    match request.get_param("auth") {
        Some(v) if v == password => (),
        Some(_) => {
            error!("Bad password");
            return Response::text("Not authorised").with_status_code(401);
        }
        None => {
            error!("Missing password");
            return Response::text("Not authorised").with_status_code(401);
        }
    }

    let mut lockedlist = match data.lock() {
        Ok(l) => l,
        Err(e) => {
            return Response::text(format!("Error: {:#?}", e)).with_status_code(500);
        }
    };

    let mut tmp_list = Vec::new();
    for v in (*lockedlist).drain(..) {
        if v > Instant::now() - Duration::from_secs(24 * 60 * 60) {
            tmp_list.push(v);
        }
    }

    for v in tmp_list {
        lockedlist.push(v);
    }

    if lockedlist.len() > 10 {
        // Overloaded
        error!("Overloaded");
        return Response::text("Too many requests").with_status_code(503);
    }

    lockedlist.push(Instant::now());

    match flash() {
        Ok(()) => Response::empty_204(),
        Err(e) => {
            error!("Failed to flash due to {:#?}", e);
            Response::text("flash error").with_status_code(500)
        }
    }
}

fn toggle() -> Result<()> {
    let room_id = std::env::var("ROOM_ID").context("Unable to get ROOM_ID environment variable")?;
    let app_token =
        std::env::var("LIFX_TOKEN").context("Unable to get LIFX_TOKEN environment variable")?;

    let url = format!("https://api.lifx.com/v1/lights/group_id:{}/toggle", room_id);
    let resp = ureq::post(&url)
        .auth(&app_token, "")
        .send_json(json!({ "duration": 0.1, }));

    if resp.ok() {
        Ok(())
    } else {
        error!("error {}: {}", resp.status(), resp.into_string()?);
        Err(anyhow!("Request failed!"))
    }
}

fn flash() -> Result<()> {
    toggle().context("Error during first toggle")?;
    toggle().context("Error during second toggle")?;
    Ok(())
}
